/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2024, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <kr2_program_api/api_v1/bundles/custom_device.h>

#include <thread>
#include <atomic>
#include <vector>
#include <unordered_map>

#define SOCKET_TIMEOUT 1

// COMMANDS

namespace kswx_asyril_devices {

    class AsyrilEyep : public kr2_bundle_api::CustomDevice {
    public:

        AsyrilEyep(boost::shared_ptr<kr2_program_api::ProgramInterface> api,
                           const boost::property_tree::ptree &xml_bundle_node);
        
        virtual ~AsyrilEyep();
        
        //
        //
        // Mandatory event methods
        //
        //
        
        //! Called by the CBun Sandbox process on instantiation (master).
        virtual int onCreate();
        //! Called by the CBun Sandbox process on erase (master).
        virtual int onDestroy();
        
        //! Called by the User Spawner process on instantiation (slave).
        virtual int onBind();
        //! Called by the User Spawner process on instantiation (slave).
        virtual int onUnbind();

        void onHWReady(const kr2_signal::HWReady&);

        //
        //
        // Published custom methods, accessible from the CBun interface
        //
        //

        //! Fetch the results of the findObjects() method.
        virtual CBUN_PCALL start(int state_id, const kr2_program_api::Number &recipe_id);

        virtual CBUN_PCALL stop(int state_id);

        virtual CBUN_PCALL getRecipeList(kr2_program_api::Array<kr2_program_api::Number> &recipes);

        virtual CBUN_PCALL getPart(kr2_program_api::Array<kr2_program_api::Number> &x, kr2_program_api::Array<kr2_program_api::Number> &y, kr2_program_api::Array<kr2_program_api::Number> &rz, boost::optional<kr2_program_api::Number> status);
 
        virtual CBUN_PCALL preparePart();

        virtual CBUN_PCALL getParameter(const std::string &parameter, kr2_program_api::Number &value);

        virtual CBUN_PCALL setParameter(const std::string &parameter, const std::string &value);

        virtual CBUN_PCALL canTakeImage(bool value);

        virtual CBUN_PCALL forceTakeImage();

        virtual CBUN_PCALL feeder(const std::string &command);

        virtual CBUN_PCALL purge(int command, int duration);

        virtual CBUN_PCALL help(const std::string &command);

        virtual CBUN_PCALL calibrate(kr2_program_api::Number &accuracy);

        virtual CBUN_PCALL getCalibrationPoint(const kr2_program_api::Number &number, kr2_program_api::Number &x, kr2_program_api::Number &y);

        virtual CBUN_PCALL saveCalibration();

        virtual CBUN_PCALL setCalibrationPoint(const kr2_program_api::Number &number, kr2_program_api::Number &x, kr2_program_api::Number &y);

        virtual CBUN_PCALL takeCalibrationImage(const kr2_program_api::Number &number);

        virtual CBUN_PCALL testCalibration(kr2_program_api::Array<kr2_program_api::Number, kr2_program_api::Number, kr2_program_api::Number> &parts_coords);
    protected:
        
        // fixed custom device methods
        
        virtual CBUN_PCALL onActivate(const boost::property_tree::ptree &param_tree);
        virtual CBUN_PCALL onDeactivate();
        
        virtual CBUN_PCALL onMount(const boost::property_tree::ptree &param_tree);
        virtual CBUN_PCALL onUnmount();
        
    private:
        
        struct Internals;
        boost::shared_ptr<Internals> internals_;
        std::unordered_map<std::string, int> state_map_ = {{"initialization", 0}, {"error", 1}, {"system_migration", 2},
                                                           {"ready", 3}, {"recipe_edition" ,4}, {"production", 5}, {"camera_configuration", 5}, {"handeye_calibration", 6},
                                                           {"backup", 7}, {"restore", 8}};

        // Standard socket allocation and configuration.
        int connectEyep();
        // Wrap and send the Keyence command
        long sendCommand(const std::string &command, std::string &errmsg);
        // Template function for commands consisting of simple command and return code response - to ommit code repetitions
        bool codeResponseCommand(const std::string &command, int &err_code, std::string &errmsg);

        // Reset buffer data.
        void recvBufferReset();
        // Receive and response data
        long recvResponse(std::string &response, std::string &errmsg);

        bool processActivationParams(const boost::property_tree::ptree &tree);
    };
}
