/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2024, Kassow Robots
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Kassow Robots nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include <asyril_devices/asyril_eyep.h>

#include <kr2_program_api/internals/api/console.h>
#include <kr2_program_api/api_v1/bundles/arg_provider_xml.h>

#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>

#include <vector>
#include <sstream>
#include <unistd.h>
#include <deque>

#include <boost/asio.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/clamp.hpp>

#include <boost/interprocess/sync/named_mutex.hpp>

using namespace kswx_asyril_devices;

#define EYEP_CMD_START_PRODUCTION                "start production"
#define EYEP_CMD_STOP_PRODUCTION                 "stop production"
#define EYEP_CMD_START_RECIPE_EDITION            "start recipe_edition"
#define EYEP_CMD_STOP_RECIPE_EDITION             "stop recipe_edition"
#define EYEP_CMD_START_CAMERA_CONFIGURATION      "start camera_configuration"
#define EYEP_CMD_STOP_CAMERA_CONFIGURATION       "stop camera_configuration"
#define EYEP_CMD_START_HANDEYE_CALIBRATION       "start handeye_calibration"
#define EYEP_CMD_STOP_HANDEYE_CALIBRATION        "stop handeye_calibration"
#define EYEP_CMD_START_RECIPE_QUALIFICATION      "start recipe_qualification"
#define EYEP_CMD_STOP_RECIPE_QUALIFICATION       "stop recipe_qualification"
#define EYEP_CMD_START_PURGE                     "start purge"
#define EYEP_CMD_STOP_PURGE                      "stop purge"
#define EYEP_CMD_GET_RECIPE_LIST                 "get_recipe_list"
#define EYEP_CMD_GET_PART                        "get_part"
#define EYEP_CMD_PREPARE_PART                    "prepare_part"
#define EYEP_CMD_GET_PARAMETER                   "get_parameter"
#define EYEP_CMD_SET_PARAMETER                   "set_parameter"
#define EYEP_CMD_CAN_TAKE_IMAGE                  "can_take_image"
#define EYEP_CMD_FORCE_TAKE_IMAGE                "force_take_image"
#define EYEP_CMD_FEEDER                          "feeder"
#define EYEP_CMD_PURGE_FULL                      "purge full"
#define EYEP_CMD_PURGE_PLATE                     "purge plate"
#define EYEP_CMD_HELP                            "help"
#define EYEP_CMD_CALIBRATE                       "calibrate"
#define EYEP_CMD_GET_CALIBRATION_POINT           "get_calibration_point"
#define EYEP_CMD_SAVE_CALIBRATION                "save_calibration"
#define EYEP_CMD_SET_CALIBRATION_POINT           "set_calibration_point"
#define EYEP_CMD_TAKE_CALIBRATION_IMAGE          "take_calibration_image"
#define EYEP_CMD_TEST_CALIBRATION                "test_calibration"

#define EYEP_CMD_ID_START_PRODUCTION                0
#define EYEP_CMD_ID_STOP_PRODUCTION                 1
#define EYEP_CMD_ID_START_RECIPE_EDITION            2
#define EYEP_CMD_ID_STOP_RECIPE_EDITION             3
#define EYEP_CMD_ID_START_CAMERA_CONFIGURATION      4
#define EYEP_CMD_ID_STOP_CAMERA_CONFIGURATION       5
#define EYEP_CMD_ID_START_HANDEYE_CALIBRATION       6
#define EYEP_CMD_ID_STOP_HANDEYE_CALIBRATION        7
#define EYEP_CMD_ID_START_RECIPE_QUALIFICATION      8
#define EYEP_CMD_ID_STOP_RECIPE_QUALIFICATION       9
#define EYEP_CMD_ID_START_PURGE                     10
#define EYEP_CMD_ID_STOP_PURGE                      11
#define EYEP_CMD_ID_GET_RECIPE_LIST                 12
#define EYEP_CMD_ID_GET_PART                        13
#define EYEP_CMD_ID_PREPARE_PART                    14
#define EYEP_CMD_ID_GET_PARAMETER                   15
#define EYEP_CMD_ID_SET_PARAMETER                   16
#define EYEP_CMD_ID_CAN_TAKE_IMAGE                  17
#define EYEP_CMD_ID_FORCE_TAKE_IMAGE                18
#define EYEP_CMD_ID_FEEDER                          19
#define EYEP_CMD_ID_PURGE_FULL                      20
#define EYEP_CMD_ID_PURGE_PLATE                     21
#define EYEP_CMD_ID_HELP                            22

#define EYEP_CODE_OK                200
#define EYEP_CODE_RESPONSE          201

#define EYEP_PARAM_STATE                       "state"
#define EYEP_PARAM_RECIPE                      "recipe"
#define EYEP_PARAM_TIMEOUT                     "timeout"
#define EYEP_PARAM_IMAGE_AFTER_SEND            "image_after_send"
#define EYEP_PARAM_IS_PREPARED                 "is_prepared"
#define EYEP_PARAM_N_PARTS_PREPARED            "n_parts_prepared"
#define EYEP_PARAM_PART_QUANTITY               "part_quantity"
#define EYEP_PARAM_VERSION                     "version"
#define EYEP_PARAM_IS_ANALYSIS_RUNNING         "is_analysis_running"

#define EYEP_STATE_HANDEYE_CONFIGURATION       "initialization"
#define EYEP_STATE_ERROR                       "error"
#define EYEP_STATE_HANDEYE_CONFIGURATION       "system_migration"
#define EYEP_STATE_HANDEYE_CONFIGURATION       "ready"
#define EYEP_STATE_RECIPE_EDITION              "recipe_edition"
#define EYEP_STATE_PRODUCTION                  "production"
#define EYEP_STATE_CAMERA_CONFIGURATION        "camera_configuration"
#define EYEP_STATE_HANDEYE_CONFIGURATION       "handeye_configuration"
#define EYEP_STATE_PRODUCTION                  "sytem_upgrade"
#define EYEP_STATE_PRODUCTION                  "recipe_qualification"
#define EYEP_STATE_PRODUCTION                  "purge"
#define EYEP_STATE_PRODUCTION                  "backup/restore"

struct AsyrilEyep::Internals
{
    boost::asio::io_service io_service_;
    boost::shared_ptr<boost::asio::ip::tcp::socket> socket_;
    
    std::deque<std::string> lines_;
    std::string last_response_="";
    
    std::string dev_ip_address_ = "";
    std::string dev_port_ = "";
};

//
// CBun mandatory methods
//

REGISTER_CLASS(kswx_asyril_devices::AsyrilEyep)

AsyrilEyep::AsyrilEyep(boost::shared_ptr<kr2_program_api::ProgramInterface> a_api,
                                       const boost::property_tree::ptree &a_xml_bundle_node)
:   kr2_bundle_api::CustomDevice(a_api, a_xml_bundle_node),
    internals_(new Internals)
{
    //REGISTER_RPC(&KeyenceCamera::triggerImage, this);
    //REGISTER_RPC(&KeyenceCamera::triggerImage, this, ARG_NUMBER(0));
    //REGISTER_RPC(&KeyenceCamera::getObjectPose, this, ARG_RPOSE(0), ARG_INT(1));
}

AsyrilEyep::~AsyrilEyep()
{
}

int AsyrilEyep::onCreate()
{

    //SUBSCRIBE(kr2_signal::HWReady, AsyrilEyep::onHWReady);
    return 0;
}

int AsyrilEyep::onDestroy()
{
    if (internals_->socket_) {
        internals_->socket_->close();
        internals_->socket_.reset();
    }
    return 0;
}

int AsyrilEyep::onBind()
{
    // process activation params
    if (activation_tree_ && !processActivationParams(*activation_tree_)) {
        return -1; // CBUN_PCALL_RET_ERROR(-1, "Invalid activation params");
    }
    
    // open device connection
    if (connectEyep() != 0) {
        return -2; // CBUN_PCALL_RET_ERROR(-2, "Failed to open connection");
    }
    
    return 0;
}

int AsyrilEyep::onUnbind()
{
    // stop production if in process
    stop(EYEP_CMD_ID_STOP_PRODUCTION);

    if (internals_->socket_) {
        internals_->socket_->close();
        internals_->socket_.reset();
    }
    return 0;
}

CBUN_PCALL AsyrilEyep::onMount(const boost::property_tree::ptree &a_param_tree)
{
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL AsyrilEyep::onUnmount()
{
    CBUN_PCALL_RET_OK;
}



CBUN_PCALL AsyrilEyep::onActivate(const boost::property_tree::ptree &a_param_tree)
{
    // process activation params
    if (!processActivationParams(a_param_tree)) {
        CBUN_PCALL_RET_ERROR(-1, "Invalid activation params");
    }
    
    // test the device connection
    if (connectEyep() != 0) {
        CBUN_PCALL_RET_ERROR(-2, "Failed to open connection");
    }

    /*
    if (internals_->socket_) {
        internals_->socket_->close();
        internals_->socket_.reset();
    }*/
    
    CBUN_PCALL_RET_OK;
}

CBUN_PCALL AsyrilEyep::onDeactivate()
{
    /*
    if (internals_->socket_) {
        internals_->socket_->close();
        internals_->socket_.reset();
    }
    */
    CBUN_PCALL_RET_OK;
}


int AsyrilEyep::connectEyep()
{
    try
    {
        using boost::asio::ip::tcp;
        
        tcp::resolver resolver(internals_->io_service_);
        tcp::resolver::query query(tcp::v4(), internals_->dev_ip_address_, internals_->dev_port_);
        tcp::resolver::iterator iterator = resolver.resolve(query);

        internals_->socket_.reset(new boost::asio::ip::tcp::socket(internals_->io_service_));

        boost::asio::connect(*internals_->socket_, iterator);
    }
    catch (std::exception& e)
    {
        CLOG_ERR("Failed to create Asyril Camera instance (unable to connect)");
        return -1;
    }

    return 0;
}

void AsyrilEyep::onHWReady(const kr2_signal::HWReady &a_event)
{
    if (activation_tree_) {
        kr2_program_api::CmdResult<> result = activate(*activation_tree_);

        switch (result.result_) {
            case kr2_program_api::CmdResult<>::EXCEPTION:
                PUBLISH_EXCEPTION(result.code_, result.message_)
                break;
            case kr2_program_api::CmdResult<>::ERROR:
                PUBLISH_ERROR(result.code_, result.message_)
                break;
        }
    }
}

bool AsyrilEyep::processActivationParams(const boost::property_tree::ptree &a_param_tree)
{
    kr2_bundle_api::ArgProviderXml arg_provider(a_param_tree);
    if (arg_provider.getArgCount() != 2) {
        CLOG_ERR("[CBUN/Keyence] Unexpected param count: actual=" << arg_provider.getArgCount() << ", expected=" << 3);
        return false;
    }
    
    internals_->dev_ip_address_ = arg_provider.getString(0);
    internals_->dev_port_ = arg_provider.getString(1);
    
    return true;
}


/** ------------------------------------------------------------------------------------------
 *
 * Socket send command (CRLF aded)
 *
 * ------------------------------------------------------------------------------------------ */
long AsyrilEyep::sendCommand(const std::string &a_command, std::string &a_errmsg)
{
    try {
        std::string cmd_send{a_command};
        cmd_send.push_back(0x0A);
        boost::asio::write(*internals_->socket_, boost::asio::buffer(cmd_send.c_str(), cmd_send.length()));
    } catch (...) {
        CLOG_WARN("Socket write failed (exception)");
        a_errmsg = "Socket write failed (exception)";
        return 1;
    }
    return 0;
}


/* ------------------------------------------------------------------------------------------
 *
 * Receive and buffer the socket response (delimited by CRLF)
 *
 * ------------------------------------------------------------------------------------------ */
long AsyrilEyep::recvResponse(std::string &a_response, std::string &a_errmsg)
{
    if (internals_->lines_.size()) {
        a_response = internals_->lines_.front();
        internals_->lines_.pop_front();
        return 0;
    }
    
    try {
        boost::asio::streambuf buff;
        boost::asio::read_until(*internals_->socket_, buff, "\n");
        
        //boost::asio::streambuf::const_buffers_type bufs = b.data();
        std::istream is(&buff);
        
        while(!is.eof()) {
            std::string line_data;
            std::getline(is, line_data);
            if (line_data.empty()) continue;
            
            if (line_data.size() && line_data[line_data.size() - 1] == '\n') line_data.pop_back();
            internals_->lines_.push_back(line_data);
        }
        
        a_response = internals_->lines_.front();
        internals_->lines_.pop_front();
        
        //std::string str_data(boost::asio::buffers_begin(buff),
        //                     boost::asio::buffers_begin(buff) + buff.size());
        //a_response = str_data;
        
    } catch (...) {
        CLOG_WARN("Recv failed (exception)");
        a_errmsg = "Message receive failed";
        return 1;
    }
    return 0;
}

void AsyrilEyep::recvBufferReset()
{
    internals_->lines_.clear();
}


/* ------------------------------------------------------------------------------------------
 *
 * Some helper fns.
 *
 * ------------------------------------------------------------------------------------------ */

std::vector<long> extractLongs(const std::string& input)
{
    std::vector<std::string> v;
    split(v, input, boost::is_any_of(" "));
    
    std::vector<long> output;
    for (auto & it : v) {
        output.push_back(atol(it.c_str()));
    }
    return output;
}

std::vector<double> extractDoubles(const std::string& input)
{
    std::vector<std::string> v;
    split(v, input, boost::is_any_of(" "));
    
    std::vector<double> output;
    for (auto & it : v) {
        output.push_back(atof(it.c_str()));
    }
    return output;
}


double extractKVDouble(const std::string& kv, std::string key)
{
    std::vector<std::string> v;
    split(v, kv, boost::is_any_of("="));
    
    if(v.size() != 2 || v[0] != key) return NAN;
    
    return atof(v[1].c_str());
}

void replaceStr(std::string& s, const std::string& from, const std::string& to){
    std::size_t start_pos = 0;
    while((start_pos = s.find(from, start_pos)) != std::string::npos) {
        s.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
}
void markdownString(std::string& s)
{
    replaceStr(s, "<", "\\<");
    replaceStr(s, ">", "\\>");
    replaceStr(s, "\n", "<br>");
    s[s.size() - 1] = ' ';
}

/** ------------------------------------------------------------------------------------------
 *
 * CBUN Published method, Eye+
 *
 * 'start production' request
 *
 * ------------------------------------------------------------------------------------------ */
CBUN_PCALL AsyrilEyep::start(int a_state_id, const kr2_program_api::Number &recipe_id)
{
    std::string errmsg; 
    int err_code;
    bool response;

    if (a_state_id == EYEP_CMD_ID_START_PRODUCTION) {
        std::string req_str = std::string(EYEP_CMD_START_PRODUCTION) + " " + std::to_string(recipe_id.l());
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else if (a_state_id == EYEP_CMD_ID_START_RECIPE_EDITION) {
        std::string req_str = std::string(EYEP_CMD_START_RECIPE_EDITION) + " " + std::to_string(recipe_id.l());
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else if (a_state_id == EYEP_CMD_ID_START_CAMERA_CONFIGURATION) {
        std::string req_str = std::string(EYEP_CMD_START_CAMERA_CONFIGURATION) + " " + std::to_string(recipe_id.l());
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else if (a_state_id == EYEP_CMD_ID_START_HANDEYE_CALIBRATION) {
        std::string req_str = std::string(EYEP_CMD_START_HANDEYE_CALIBRATION) + " " + std::to_string(recipe_id.l());
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else if (a_state_id == EYEP_CMD_ID_START_RECIPE_QUALIFICATION) {
        std::string req_str = std::string(EYEP_CMD_START_RECIPE_QUALIFICATION) + " " + std::to_string(recipe_id.l());
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else if (a_state_id == EYEP_CMD_ID_START_PURGE) {
        std::string req_str = std::string(EYEP_CMD_START_PURGE) + " " + std::to_string(recipe_id.l());
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else {
        response = false;
        err_code = 1005;
        errmsg = "Invalid Eye+ command!";
    }

    if (response) { CBUN_PCALL_RET_OK; } else { CBUN_PCALL_RET_ERROR(err_code, errmsg); }
}


CBUN_PCALL AsyrilEyep::stop(int a_state_id)
{
    std::string errmsg; 
    int err_code;
    bool response;

    if (a_state_id == EYEP_CMD_ID_STOP_PRODUCTION) {
        std::string req_str = std::string(EYEP_CMD_STOP_PRODUCTION);
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else if (a_state_id == EYEP_CMD_ID_STOP_RECIPE_EDITION) {
        std::string req_str = std::string(EYEP_CMD_STOP_RECIPE_EDITION);
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else if (a_state_id == EYEP_CMD_ID_STOP_CAMERA_CONFIGURATION) {
        std::string req_str = std::string(EYEP_CMD_STOP_CAMERA_CONFIGURATION);
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else if (a_state_id == EYEP_CMD_ID_STOP_HANDEYE_CALIBRATION) {
        std::string req_str = std::string(EYEP_CMD_STOP_HANDEYE_CALIBRATION);
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else if (a_state_id == EYEP_CMD_ID_STOP_RECIPE_QUALIFICATION) {
        std::string req_str = std::string(EYEP_CMD_STOP_RECIPE_QUALIFICATION);
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else if (a_state_id == EYEP_CMD_ID_STOP_PURGE) {
        std::string req_str = std::string(EYEP_CMD_STOP_PURGE);
        response = codeResponseCommand(req_str, err_code, errmsg);
    } else {
        response = false;
        err_code = 1005;
        errmsg = "Invalid Eye+ command!";
    }

    if (response) { CBUN_PCALL_RET_OK; } else { CBUN_PCALL_RET_ERROR(err_code, errmsg); }
}

CBUN_PCALL AsyrilEyep::getRecipeList(kr2_program_api::Array<kr2_program_api::Number> &a_recipes) 
{
    if (!internals_->socket_) { CBUN_PCALL_RET_ERROR(3, "Eye+ not connected"); }

    std::string err_msg, req_str;
    
    req_str = std::string(EYEP_CMD_GET_RECIPE_LIST);
    if (sendCommand(req_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1001, err_msg.c_str()); }

    // Reading all response lines     
    std::vector<std::string> response_lines;
    std::string resp_str;

    recvBufferReset();
    if (recvResponse(resp_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1002, err_msg.c_str()); }
    response_lines.push_back(resp_str);

    while (internals_->lines_.size() > 0) {
        if (recvResponse(resp_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1002, err_msg.c_str()); }
        response_lines.push_back(resp_str);
    }

    std::vector<std::string> items;
    split(items, response_lines[0], boost::is_any_of(" "));
    int exit_code = atol(items[0].c_str());
    if (exit_code != EYEP_CODE_RESPONSE) {CBUN_PCALL_RET_ERROR(atol(items[0].c_str()), "Eye+ error code received");}
    int num_of_recipes = atol(items[1].c_str());
    if (num_of_recipes == 0) { CBUN_PCALL_RET_OK; }
    long first_recipe = atol(items[2].c_str());
    a_recipes.item<kr2_program_api::Number>(0, 0) = first_recipe;

    for (int i = 1; i < num_of_recipes; i++) {
        std::vector<std::string> items;
        split(items, response_lines[i], boost::is_any_of(" "));
        long recipe = atol(items[0].c_str());
        a_recipes.item<kr2_program_api::Number>(i, 0) = recipe;
    }

    CBUN_PCALL_RET_OK; 
}

CBUN_PCALL AsyrilEyep::getPart(kr2_program_api::Array<kr2_program_api::Number> &a_out_x, kr2_program_api::Array<kr2_program_api::Number> &a_out_y, kr2_program_api::Array<kr2_program_api::Number> &a_out_rz, boost::optional<kr2_program_api::Number> status)
{
    if (!internals_->socket_) { CBUN_PCALL_RET_ERROR(3, "Eye+ not connected"); }
    std::string err_msg;
    
    std::string req_str = std::string(EYEP_CMD_GET_PART);
    if (sendCommand(req_str, err_msg) != 0) {
        CBUN_PCALL_RET_ERROR(1001, err_msg.c_str());
    }
    
    std::string resp_str;
    recvBufferReset();
    if (recvResponse(resp_str, err_msg) != 0) {
        CBUN_PCALL_RET_ERROR(1002, err_msg.c_str());
    }

    std::cout<< resp_str << '\n';

    std::vector<std::string> items;
    split(items, resp_str, boost::is_any_of(" "));
    if (items.size() == 0) { CBUN_PCALL_RET_ERROR(1003, "Invaliud Eye+ response"); }
    if (items.size() >= 1 && atol(items[0].c_str()) != EYEP_CODE_OK) {
        if (status && status->valid()) {
            status = kr2_program_api::Number(long(1)); // error status
            CBUN_PCALL_RET_OK;
        }
        else{
          CBUN_PCALL_RET_ERROR(atol(items[0].c_str()), "Eye+ error code received");  
        }
    }

    if ((items.size() - 1) % 3 != 0) { CBUN_PCALL_RET_ERROR(1004, "Unexpected data format"); }

    if (status && status->valid()) {
        status = kr2_program_api::Number(long(0)); // success
    }
    
    double xyrz[items.size() - 1];
    int num_parts = (items.size() - 1) / 3;
    for (int i = 0; i < num_parts; i++){
        xyrz[3 * i + 0] = extractKVDouble(items[i * 3 + 1], "x");
        xyrz[3 * i + 1] = extractKVDouble(items[i * 3 + 2], "y");
        xyrz[3 * i + 2] = extractKVDouble(items[i * 3 + 3], "rz");
    }
    // xyrz[0] = extractKVDouble(items[1], "x");
    // xyrz[1] = extractKVDouble(items[2], "y");
    // xyrz[2] = extractKVDouble(items[3], "rz");
    
    for (int i = 0; i < num_parts; i++){
        a_out_x.item<kr2_program_api::Number>(i, 0) = xyrz[3 * i];
        a_out_y.item<kr2_program_api::Number>(i, 0) = xyrz[3 * i + 1];
        a_out_rz.item<kr2_program_api::Number>(i, 0) = xyrz[3 * i + 2];
    }

    CBUN_PCALL_RET_OK;
}

CBUN_PCALL AsyrilEyep::preparePart()
{
    std::string req_str = std::string(EYEP_CMD_PREPARE_PART);
    std::string errmsg; 
    int err_code;
    bool response;

    response = codeResponseCommand(req_str, err_code, errmsg);

    if (response) { CBUN_PCALL_RET_OK; } else { CBUN_PCALL_RET_ERROR(err_code, errmsg); }
}

CBUN_PCALL AsyrilEyep::getParameter(const std::string &a_parameter, kr2_program_api::Number &a_value)
{
    if (!internals_->socket_) { CBUN_PCALL_RET_ERROR(3, "Eye+ not connected"); }
    
    std::string err_msg, req_str;
    
    req_str = std::string(EYEP_CMD_GET_PARAMETER) + " " + a_parameter;
    if (sendCommand(req_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1001, err_msg.c_str()); }
    
    std::string resp_str;
    recvBufferReset();
    if (recvResponse(resp_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1002, err_msg.c_str()); }

    std::vector<std::string> items;
    split(items, resp_str, boost::is_any_of(" "));
    if ((items.size() == 2 || items.size() == 3) && atol(items[0].c_str()) == EYEP_CODE_OK) { 
        if (a_parameter == EYEP_PARAM_STATE && state_map_.find(items[1]) != state_map_.end()) {
            a_value = (long)state_map_[items[1]];
        } else if (a_parameter == EYEP_PARAM_IMAGE_AFTER_SEND || a_parameter == EYEP_PARAM_IS_PREPARED || a_parameter == EYEP_PARAM_IS_ANALYSIS_RUNNING) {
            if (items[1] == "true") {
                a_value = 1L;
            } else {
                a_value = 0L;
            }
        } else if (a_parameter == EYEP_PARAM_VERSION) {
                std::size_t found = items[1].find_last_of(".");
                a_value = atof(items[1].substr(0,found).c_str());
        } else {
            a_value = atof(items[1].c_str());
        }

        CBUN_PCALL_RET_OK; 
    }

    if (items.size() > 3) { CBUN_PCALL_RET_ERROR(atol(items[0].c_str()), "Eye+ error code received"); }
    
    CBUN_PCALL_RET_ERROR(1003, "Invaliud Eye+ response!");
}

CBUN_PCALL AsyrilEyep::setParameter(const std::string &a_parameter, const std::string &a_value)
{
    std::string req_str = std::string(EYEP_CMD_SET_PARAMETER) + " " + a_parameter + " " + a_value;
    std::string errmsg; 
    int err_code;
    bool response;

    response = codeResponseCommand(req_str, err_code, errmsg);

    if (response) { CBUN_PCALL_RET_OK; } else {
        CBUN_PCALL_RET_ERROR(err_code, errmsg);
    }
}

CBUN_PCALL AsyrilEyep::canTakeImage(bool a_value)
{
    std::string req_str;
    std::string errmsg; 
    int err_code;
    bool response;

    if (a_value) {
        req_str = std::string(EYEP_CMD_CAN_TAKE_IMAGE) + " true";
    } else {
        req_str = std::string(EYEP_CMD_CAN_TAKE_IMAGE) + " false";
    }

    response = codeResponseCommand(req_str, err_code, errmsg);

    if (response) { CBUN_PCALL_RET_OK; } else { CBUN_PCALL_RET_ERROR(err_code, errmsg); }
}

CBUN_PCALL AsyrilEyep::forceTakeImage()
{
    std::string req_str = std::string(EYEP_CMD_FORCE_TAKE_IMAGE);
    std::string errmsg; 
    int err_code;
    bool response;

    response = codeResponseCommand(req_str, err_code, errmsg);

    if (response) { CBUN_PCALL_RET_OK; } else { CBUN_PCALL_RET_ERROR(err_code, errmsg); }
}

CBUN_PCALL AsyrilEyep::feeder(const std::string &a_command)
{
    std::string req_str = std::string(EYEP_CMD_FEEDER) + " " + a_command;
    std::string errmsg; 
    int err_code;
    bool response;

    response = codeResponseCommand(req_str, err_code, errmsg);

    if (response) { CBUN_PCALL_RET_OK; } else { CBUN_PCALL_RET_ERROR(err_code, errmsg); }
}


CBUN_PCALL AsyrilEyep::purge(int a_command_id, int a_duration) 
{ 
    std::string errmsg; 
    int err_code;
    bool response;

    if (a_command_id == EYEP_CMD_ID_PURGE_FULL) {
        if (a_duration >= 1 && a_duration <= 30000) {
            std::string req_str = std::string(EYEP_CMD_PURGE_FULL) + " " + std::to_string(a_duration);
            response = codeResponseCommand(req_str, err_code, errmsg);
        } else {
            response = false;
            err_code = 1006;
            errmsg = "Invalid purge duration value, values must be in range [1, 30000]!";
        }
    } else if(a_command_id == EYEP_CMD_ID_PURGE_PLATE) {
        if (a_duration >= 1 && a_duration <= 30000) {
            std::string req_str = std::string(EYEP_CMD_PURGE_PLATE) + " " + std::to_string(a_duration);
            response = codeResponseCommand(req_str, err_code, errmsg);
        } else {
            std::string req_str = std::string(EYEP_CMD_PURGE_PLATE);
            response = codeResponseCommand(req_str, err_code, errmsg);
        }
    } else {
        response = false;
        err_code = 1005;
        errmsg = "Invalid Eye+ command!";
    }

    if (response) { CBUN_PCALL_RET_OK; } else { CBUN_PCALL_RET_ERROR(err_code, errmsg); }
}

CBUN_PCALL AsyrilEyep::help(const std::string &a_command)
{
     if (!internals_->socket_) { CBUN_PCALL_RET_ERROR(3, "Eye+ not connected"); }

    std::string err_msg, req_str;
    
    req_str = std::string(EYEP_CMD_HELP) + " " + a_command;
    if (sendCommand(req_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1001, err_msg.c_str()); }

    // Reading all response lines     
    std::string result_string;
    std::string resp_str;

    recvBufferReset();
    if (recvResponse(resp_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1002, err_msg.c_str()); }

    result_string += resp_str;

    std::vector<std::string> items;
    split(items, resp_str, boost::is_any_of(" "));
    int exit_code = atol(items[0].c_str());
    if (exit_code != EYEP_CODE_RESPONSE) {CBUN_PCALL_RET_ERROR(atol(items[0].c_str()), "Eye+ error code received");}


    while (internals_->lines_.size() > 0) {
        if (recvResponse(resp_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1002, err_msg.c_str()); }
        result_string += resp_str;
    }

    markdownString(result_string);

    // Dialog publishing
    auto dialog = kr2_program_api::OutputDialog::factory("help output", result_string);
    
    dialog->setNeutralButton("Proceed", [](){});
    dialog->show(false);

    CBUN_PCALL_RET_OK; 
}

bool AsyrilEyep::codeResponseCommand(const std::string& a_command, int &a_err_code, std::string &a_errmsg) 
{
    if (!internals_->socket_) {
        a_err_code = 3;
        a_errmsg = "Eye+ not connected";
        return false;
    }

    std::string err_msg;
    
    if (sendCommand(a_command, err_msg) != 0) {
        a_err_code = 1001;
        a_errmsg = err_msg.c_str();
        return false;
    }
    
    std::string resp_str;
    recvBufferReset();
    if (recvResponse(resp_str, err_msg) != 0) {
        a_err_code = 1002;
        a_errmsg = err_msg.c_str();
        return false;
    }

    std::vector<std::string> items;
    split(items, resp_str, boost::is_any_of(" "));
    if (items.size() == 1 && atol(items[0].c_str()) == EYEP_CODE_OK) { return true; }
    if (items.size() > 1 && atol(items[0].c_str()) == EYEP_CODE_OK) {
        CLOG_INFO(resp_str);
        return true;
    }
    if (items.size() > 1) {
        CLOG_ERR("Eye+ error received: " << resp_str);
        a_errmsg = "Eye+ error code received";
        return false;
    }
    
    a_err_code = 1003;
    a_errmsg = "Invalid Eye+ response!";
    return false;
}

CBUN_PCALL AsyrilEyep::calibrate(kr2_program_api::Number &a_accuracy)
{
    if (!internals_->socket_) { CBUN_PCALL_RET_ERROR(3, "Eye+ not connected"); }
    
    std::string err_msg, req_str;
    
    req_str = std::string(EYEP_CMD_CALIBRATE);
    if (sendCommand(req_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1001, err_msg.c_str()); }
    
    std::string resp_str;
    recvBufferReset();
    if (recvResponse(resp_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1002, err_msg.c_str()); }

    std::vector<std::string> items;
    split(items, resp_str, boost::is_any_of(" "));
    if ((items.size() == 2) && atol(items[0].c_str()) == EYEP_CODE_OK) {
        std::vector<std::string> resp_items;
        split(resp_items, items[1], boost::is_any_of("="));
        a_accuracy = atof(resp_items[1].c_str());

        CBUN_PCALL_RET_OK; 
    }

    if (items.size() > 2) { CBUN_PCALL_RET_ERROR(atol(items[0].c_str()), "Eye+ error code received"); }
    
    CBUN_PCALL_RET_ERROR(1003, "Invaliud Eye+ response!");
}

CBUN_PCALL AsyrilEyep::getCalibrationPoint(const kr2_program_api::Number &a_number, kr2_program_api::Number &a_out_x, kr2_program_api::Number &a_out_y)
{
    if (!internals_->socket_) { CBUN_PCALL_RET_ERROR(3, "Eye+ not connected"); }
    std::string err_msg;
    
    std::string req_str = std::string(EYEP_CMD_GET_CALIBRATION_POINT);
    if (a_number.l() >= 1 && a_number.l() <= 4){
        std::string req_str = std::string(EYEP_CMD_GET_CALIBRATION_POINT) + " " + std::to_string(a_number.l());
        if (sendCommand(req_str, err_msg) != 0) {
            CBUN_PCALL_RET_ERROR(1001, err_msg.c_str());
        }
    
        std::string resp_str;
        recvBufferReset();
        if (recvResponse(resp_str, err_msg) != 0) {
            CBUN_PCALL_RET_ERROR(1002, err_msg.c_str());
        }

        std::vector<std::string> items;
        split(items, resp_str, boost::is_any_of(" "));
        if (items.size() == 0) { CBUN_PCALL_RET_ERROR(1003, "Invaliud Eye+ response"); }
        if (items.size() >= 1 && atol(items[0].c_str()) != EYEP_CODE_OK) { CBUN_PCALL_RET_ERROR(atol(items[0].c_str()), "Eye+ error code received"); }
    
        if (items.size() != 3) { CBUN_PCALL_RET_ERROR(1004, "Unexpected data format"); }
    
        double xy[2];
        xy[0] = extractKVDouble(items[1], "x");
        xy[1] = extractKVDouble(items[2], "y");
    
        a_out_x = xy[0];
        a_out_y = xy[1];
    
        CBUN_PCALL_RET_OK;
    }
    else{
        CBUN_PCALL_RET_ERROR(1006, "Invalid point number value, values must be in range [1, 4]!");
    }
    
}

CBUN_PCALL AsyrilEyep::saveCalibration()
{
    std::string req_str = std::string(EYEP_CMD_SAVE_CALIBRATION);
    std::string errmsg; 
    int err_code;
    bool response;

    response = codeResponseCommand(req_str, err_code, errmsg);

    if (response) { CBUN_PCALL_RET_OK; } else { CBUN_PCALL_RET_ERROR(err_code, errmsg); }
}

CBUN_PCALL AsyrilEyep::setCalibrationPoint(const kr2_program_api::Number &a_number, kr2_program_api::Number &a_in_x, kr2_program_api::Number &a_in_y)
{
    if (a_number.l() >= 1 && a_number.l() <= 4){
        std::string req_str = std::string(EYEP_CMD_SET_CALIBRATION_POINT) + " " + std::to_string(a_number.l()) + " " + std::to_string(a_in_x.l()) + " " + std::to_string(a_in_y.l());
        std::string errmsg; 
        int err_code;
        bool response;
    

        response = codeResponseCommand(req_str, err_code, errmsg);

        if (response) { CBUN_PCALL_RET_OK; } else { CBUN_PCALL_RET_ERROR(err_code, errmsg); }
    }
    else{
        CBUN_PCALL_RET_ERROR(1006, "Invalid point number value, values must be in range [1, 4]!");
    }
    
}

CBUN_PCALL AsyrilEyep::takeCalibrationImage(const kr2_program_api::Number &a_number)
{
    if (a_number.l() >= 1 && a_number.l() <= 4){
    
        std::string req_str = std::string(EYEP_CMD_TAKE_CALIBRATION_IMAGE) + " " + std::to_string(a_number.l());
        std::string errmsg; 
        int err_code;
        bool response;

        response = codeResponseCommand(req_str, err_code, errmsg);

        if (response) { CBUN_PCALL_RET_OK; } else { CBUN_PCALL_RET_ERROR(err_code, errmsg); }
    }
    else{
        CBUN_PCALL_RET_ERROR(1006, "Invalid point number value, values must be in range [1, 4]!");
    }
}

CBUN_PCALL AsyrilEyep::testCalibration(kr2_program_api::Array<kr2_program_api::Number, kr2_program_api::Number, kr2_program_api::Number> &a_parts_coords) 
{
    if (!internals_->socket_) { CBUN_PCALL_RET_ERROR(3, "Eye+ not connected"); }

    std::string err_msg, req_str;
    
    req_str = std::string(EYEP_CMD_TEST_CALIBRATION);
    if (sendCommand(req_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1001, err_msg.c_str()); }

    // Reading all response lines     
    std::vector<std::string> response_lines;
    std::string resp_str;

    recvBufferReset();
    if (recvResponse(resp_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1002, err_msg.c_str()); }
    response_lines.push_back(resp_str);

    while (internals_->lines_.size() > 0) {
        if (recvResponse(resp_str, err_msg) != 0) { CBUN_PCALL_RET_ERROR(1002, err_msg.c_str()); }
        response_lines.push_back(resp_str);
    }

    std::vector<std::string> items;
    split(items, response_lines[0], boost::is_any_of(" "));
    int exit_code = atol(items[0].c_str());
    if (exit_code != EYEP_CODE_RESPONSE) {CBUN_PCALL_RET_ERROR(atol(items[0].c_str()), "Eye+ error code received");}
    int num_of_parts = atol(items[1].c_str());
    if (num_of_parts == 0) { CBUN_PCALL_RET_OK; }
    
    auto row = kr2_program_api::Row<kr2_program_api::Number, kr2_program_api::Number, kr2_program_api::Number>(extractKVDouble(items[2], "x"),
        extractKVDouble(items[3], "y"), extractKVDouble(items[4], "rz"));
    a_parts_coords.row(0) = row;

    for (int i = 1; i < num_of_parts; i++) {
        std::vector<std::string> items;
        split(items, response_lines[i], boost::is_any_of(" "));

        row = kr2_program_api::Row<kr2_program_api::Number, kr2_program_api::Number, kr2_program_api::Number>(extractKVDouble(items[0], "x"),
            extractKVDouble(items[1], "y"), extractKVDouble(items[2], "rz"));
        a_parts_coords.row(i) = row;
    }

    CBUN_PCALL_RET_OK; 
}